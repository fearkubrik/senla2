package Storage;

import entity.Customer;

public class CustomerStorage extends AbstractDataStorage<Customer>{

    private static CustomerStorage instance;

    private CustomerStorage() {

    }

    public static CustomerStorage getInstance() {
        if(instance == null){
            instance = new CustomerStorage();
        }
        return instance;
    }
}
