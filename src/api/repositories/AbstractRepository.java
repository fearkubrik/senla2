package api.repositories;

import entity.BaseEntity;

import java.util.List;
import java.util.Optional;

public interface AbstractRepository<T extends BaseEntity> {

    Long create(T entity);

    void delete(Long id);

    void update(T entity);

    Optional<T> get(Long id);

    List<T> getAll();
}
