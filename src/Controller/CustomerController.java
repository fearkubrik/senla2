package Controller;

import Services.CustomerServiceImpl;
import api.services.CustomerService;
import entity.Customer;

public class CustomerController extends AbstractController<Customer, CustomerService>{

    private static CustomerController instance;

    private CustomerController() {
        super(CustomerServiceImpl.getInstance());
    }

    public static CustomerController getInstance() {
        if(instance == null){
            instance = new CustomerController();
        }
        return instance;
    }
}
