package Controller;

import api.services.AbstractService;
import entity.BaseEntity;

public abstract class AbstractController<E extends BaseEntity, S extends AbstractService<E>> {

    protected S service;

    protected AbstractController(S service){
        this.service = service;
    }

    public Long create(E entity) {
        return service.create(entity);
    }

    public void delete(Long id){
        service.delete(id);
    }

    public void update(E entity){
        service.update(entity);
    }

    public E get(Long id) {
        return service.get(id);
    }
}
