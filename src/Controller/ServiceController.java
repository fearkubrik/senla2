package Controller;

import Services.ServicesServiceImpl;
import api.services.ServicesService;
import entity.Service;

public class ServiceController extends AbstractController<Service, ServicesService>{

    private static ServiceController instance;

    private ServiceController(){
        super(ServicesServiceImpl.getInstance());
    }

    public static ServiceController getInstance() {
        if(instance == null){
            instance = new ServiceController();
        }
        return instance;
    }
}
