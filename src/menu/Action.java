package menu;

public interface Action {
    public void execute();
}
