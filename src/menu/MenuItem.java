package menu;

public class MenuItem {

    private String key;
    private String title;
    private Action action;
    private MenuItem subMenu;


    public String getKey() { return key;}

    public String getTitle() { return title;}

    public MenuItem getSubMenu() { return subMenu;}


    public MenuItem(String key, String title, Action action) {
        this.key = key;
        this.title = title;
        this.action = action;
        this.subMenu = subMenu;

    }

    public void executeAction() {
        action.execute();
    }

}
