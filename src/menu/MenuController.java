package menu;

public class MenuController {
    private MenuBuilder builder;
    private Navigator navigator;

    private static MenuController instance;

    private MenuController() {
        this.builder = MenuBuilder.getInstance();
        this.navigator = Navigator.getInstance();
    }

    public static MenuController getInstance() {
        if (instance == null) {
            instance = new MenuController();
        }
        return instance;
    }

    public void run() {
        while (true) {
            navigator.printMenu();
            navigator.navigate();
        }
    }
}
