package menu;

import java.util.ArrayList;
import java.util.List;

public class Menu {

    private List<MenuItem> menuItems = new ArrayList<>();
    private String name;

    public String getName() { return name;}

    public List<MenuItem> getMenuItems(){ return menuItems;}

    public Menu(String name) {
        this.name = name;
    }

    public Menu(List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public Menu(String name, List<MenuItem> menuItems) {
        this.name = name;
        this.menuItems = menuItems;
    }
}
