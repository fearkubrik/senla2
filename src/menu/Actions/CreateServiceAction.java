package menu.Actions;

import context.Context;
import entity.Service;
import menu.Action;

public class CreateServiceAction implements Action {

    @Override
    public void execute(){
        int price;
        Service service = new Service();
        System.out.print("Введите цену: ");
        price = Context.key.nextInt();
        service.setPrices(price);
        Context.getInstance().services.add(service);
    }

}
