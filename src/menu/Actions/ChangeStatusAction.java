package menu.Actions;

import context.Context;
import entity.Apartments;
import entity.enums.Status;
import menu.Action;

public class ChangeStatusAction implements Action {

    @Override
    public void execute(){

        int id;
        System.out.print("Введите ID апартаментов: ");
        id = Context.key.nextInt();

        for (Apartments apartments : Context.getInstance().apartments){
            if (apartments.getRoomId() == id && apartments.getStatus() == Status.Free){
                apartments.setStatus(Status.Service);
                return;
            }
        }


    }

}
