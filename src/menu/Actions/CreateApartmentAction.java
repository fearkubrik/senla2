package menu.Actions;

import context.Context;
import entity.Apartments;
import entity.enums.Status;
import menu.Action;

public class CreateApartmentAction implements Action {

    @Override
    public void execute(){

        int price;
        Apartments apartment = new Apartments();
        System.out.print("Введите цену: ");
        price = Context.key.nextInt();
        apartment.setPrice(price);
        apartment.setStatus(Status.Free);
        Context.getInstance().apartments.add(apartment);

    }

}
