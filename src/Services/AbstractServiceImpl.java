package Services;

import api.repositories.AbstractRepository;
import api.services.AbstractService;
import entity.BaseEntity;

public abstract class AbstractServiceImpl<E extends BaseEntity, R extends AbstractRepository<E>> implements AbstractService<E> {

    protected R repository;

    protected AbstractServiceImpl(R repository){
        this.repository = repository;
    }

    @Override
    public Long create(E entity){
        return repository.create(entity);
    }

    @Override
    public void delete(Long id){
        repository.delete(id);
    }

    @Override
    public void update(E entity) {
        repository.update(entity);
    }

    @Override
    public E get(Long id){
        return repository.get(id).orElseThrow(IllegalArgumentException::new);
    }

}
