package Services;

import Repositories.HotelRepositoryImpl;
import api.repositories.HotelRepository;
import api.services.HotelService;
import entity.Apartments;

public class HotelServiceImpl extends AbstractServiceImpl<Apartments, HotelRepository> implements HotelService {

    private static HotelService instance;

    private HotelServiceImpl() {
        super(HotelRepositoryImpl.getInstance());
    }

    public static HotelService getInstance() {
        if(instance == null){
            instance = new HotelServiceImpl();
        }
        return instance;
    }


}
