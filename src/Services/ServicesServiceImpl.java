package Services;

import Repositories.ServiceRepositoryImpl;
import api.repositories.ServicesRepository;
import api.services.ServicesService;
import entity.Service;

public class ServicesServiceImpl extends AbstractServiceImpl<Service, ServicesRepository> implements ServicesService {

    private static ServicesService instance;

    private ServicesServiceImpl() {
        super(ServiceRepositoryImpl.getInstance());
    }

    public static ServicesService getInstance() {
        if (instance == null) {
            instance = new ServicesServiceImpl();
        }
        return instance;
    }
}
