package main;

import context.MainInitializer;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        MainInitializer initializer = new MainInitializer();
        initializer.init();
    }

}
