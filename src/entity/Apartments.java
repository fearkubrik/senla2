package entity;

import entity.enums.Status;

public class Apartments extends BaseEntity {


    private Customer customer;
    private int price;
    private Status status;

    public Apartments(Customer customer, int price, Status status){
        this.customer = customer;
        this.price = price;
        this.status = status;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }


}
