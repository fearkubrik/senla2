package entity;


import context.Context;

import java.util.ArrayList;

public class Service extends BaseEntity{

    private int price;
    private String name;

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Service() {
        this.name = name;
        this.price = price;
    }


}
