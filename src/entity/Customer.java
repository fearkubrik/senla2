package entity;

public class Customer extends BaseEntity{

    private String customerName;

    public Customer(String customerName){
        this.customerName = customerName;
    }

    public String getCustomerName() {
        return customerName;
    }
}
