package context;

import entity.Apartments;
import entity.Service;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Context {

    private static Context instance;
    public int counterApart = 0;
    public int counterService = 0;
    public ArrayList<Apartments> apartments;
    public ArrayList<Service> services;

    public static Scanner key = new Scanner(System.in);

    public static Context getInstance(){
        if (instance == null){
            instance = new Context();
        }
        return instance;
    }
}
