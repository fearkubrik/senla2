package Repositories;

import Storage.AbstractDataStorage;
import Storage.ApartmentsStorage;
import api.repositories.HotelRepository;
import entity.Apartments;

public class HotelRepositoryImpl extends AbstractRepositoryImpl<Apartments>  implements HotelRepository {
    private static HotelRepository instance;

    private HotelRepositoryImpl(AbstractDataStorage<Apartments> storage) {
        super(storage);
    }

    public static HotelRepository getInstance() {
        if(instance == null){
            instance = new HotelRepositoryImpl(ApartmentsStorage.getInstance());
        }
        return instance;
    }

}
