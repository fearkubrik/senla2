package Repositories;

import Storage.AbstractDataStorage;
import api.repositories.AbstractRepository;
import entity.BaseEntity;

import java.util.List;
import java.util.Optional;

public abstract class AbstractRepositoryImpl<E extends BaseEntity> implements AbstractRepository<E> {

    protected final AbstractDataStorage<E> storage;

    protected AbstractRepositoryImpl(AbstractDataStorage<E> storage){
        this.storage = storage;
    }

    @Override
    public Long create(E entity) {
        return storage.createEntity(entity).getId();
    }

    @Override
    public void delete(Long id) {
        E foungItem = storage.getEntities()
                .stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);

        storage.getEntities().remove(foungItem);
    }

    @Override
    public void update(E entity) {
        int index = storage.getEntities().indexOf(entity);
        storage.getEntities().set(index, entity);
    }

    @Override
    public Optional<E> get(Long id){
        return storage.getEntities()
                .stream()
                .filter(e -> e.getId().equals(id))
                .findFirst();
    }

    @Override
    public List<E> getAll() {
        return storage.getEntities();
    }
}
